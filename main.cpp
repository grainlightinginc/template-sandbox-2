#include <iostream>
#include <memory>

#include "Lamp.h"
#include "Models.h"

using namespace std;


using Lamp = TestLamp;
using Display = Lamp::Display;
using Frame = Lamp::Frame;


int main() {
    // Create pipeline.
    auto a = make_shared<TestFile<Frame>>(0, 1, 24);
    auto b = make_shared<Interpolator<Frame>>(a, 1);
    auto c = make_shared<Display>(b);
    auto d = make_shared<Multiplier<Frame>>(c, 1);
    auto e = make_shared<Display>(d);

    // Each line shows two outputs:
    //   one before multiplying and one after.
    cout << "with mult 1" << endl;
    e->pump();

    cout << endl << "with mult 2" << endl;
    e->restart();
    d->set_mult(2);
    e->pump();

    cout <<  endl <<"with mult 3" << endl;
    e->restart();
    d->set_mult(3);
    e->pump();
}
