#pragma once

#include <memory>

using namespace std;

template<typename T>
class Source {
public:
	Source() {}
	Source(shared_ptr<Source<T>> s) : source(s) {}

	void pump() {
		// Throw away read value for side effects.
		T t;
		read(t);
	}
	virtual bool read(T& t) = 0;

	virtual void reset_state() {}
	
	virtual void restart() {
		if (source) source->restart();
		this->reset_state();
	};

protected:
	shared_ptr<Source<T>> source;
};

template<typename T>
class Tee : public Source<T> {
public:
	Tee(shared_ptr<Source<T>> s) : Source<T>(s) {}

	virtual bool write(const T& t) = 0;

	bool read(T& t) {
		if (this->source && this->source->read(t)) {
			write(t);
			return true;
		} else {
			return false;
		}
	}

};
