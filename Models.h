#pragma once

#include "Lamp.h"


struct TestLamp {
    using Frame = RectangularFrame<2, 2>;
    using Display = ConsoleDisplay<Frame>;
};

/*

struct HorizonLamp {
    using Frame = RectangularFrame<30, 2>;
    using Display = HorizonDisplay<Frame, 3, 4>;
};

struct HorizonV2Lamp {
    using Frame = RectangularFrame<30, 2>;
    using Display = HorizonV2Display<Frame, 3, 4, 5, 6>;
};

struct HorizonV2aLamp {
    using Frame = RectangularFrame<30, 2>;
    using Display = HorizonV2Display<Frame, 6, 5, 4, 3>;
};

*/