#pragma once

#include <cstdint>
#include <cstring>
#include <iostream>
#include <memory>

#include "Pipeline.h"

using namespace std;

template<typename T, typename ...Args>
std::unique_ptr<T> make_unique( Args&& ...args ) {
    return std::unique_ptr<T>( new T( std::forward<Args>(args)... ) );
}

//
// Frames.
//
template<int c>
struct CircularFrame {
    static constexpr unsigned int frame_bytes() { return pixels() * 3; }; 
    static constexpr unsigned int pixels() { return c; }; 
    const uint8_t& operator[](unsigned int i) const { return buf[i]; };
    uint8_t& operator[](unsigned int i) { return buf[i]; };
    uint8_t buf[frame_bytes()];
};

template<int m, int n>
struct RectangularFrame {
    static constexpr unsigned int frame_bytes() { return pixels() * 3; }; 
    static constexpr unsigned int pixels() { return m * n; }; 
    static constexpr unsigned int dim_x() { return m; }; 
    static constexpr unsigned int dim_y() { return n; }; 
    const uint8_t& operator[](unsigned int i) const { return buf[i]; };
    uint8_t& operator[](unsigned int i) { return buf[i]; };
    uint8_t buf[frame_bytes()];
};



template<typename Frame>
class TestFile : public Source<Frame> {
public:
    TestFile(uint8_t s, int8_t d1, int8_t d2) :
        counter(s), delta1(d1), delta2(d2) {};
    
    bool read(Frame& frame) override {
        int _counter = counter;
        for (int i = 0; i < Frame::frame_bytes(); ++i) {
            frame[i] = _counter & 0xFF;
            _counter += delta1;
        }
        counter += delta2;
        return true;
    }
    
    void reset_state() override {
        counter = 0;
    }
private:
    uint8_t counter = 0;
    const int8_t delta1 = 0;
    const int8_t delta2 = 0;
};

template<typename Frame>
class Interpolator : public Source<Frame> {
public:
    Interpolator(shared_ptr<Source<Frame>> s,
                 unsigned int nframes, unsigned int step = 1) :
        Source<Frame>(s), interpolation_frames(nframes), step_size(step) {
        current_frame = &frame_a;
        next_frame = &frame_b;
        reset_state();
    };


    bool read(Frame& frame) override {
        uint8_t* buf = frame.buf;
        if (interpolation_pos == 0) {
          // Base case: Simply read out the current frame.
          memcpy(buf, current_frame, frame_bytes);
        } else {
          // We need to interpolate.
          uint32_t dst_mix = (interpolation_pos << 16) /
                             (interpolation_frames + 1);
          uint32_t src_mix = (1 << 16) - dst_mix;
          int i;
          for (i = 0; i < frame_bytes; i++) {
            buf[i] = (uint8_t) ((src_mix * (*current_frame)[i]
                    + dst_mix * (*next_frame)[i]) >> 16);
          }
        }
        interpolation_pos += step_size;
        while (interpolation_pos > interpolation_frames) {
          // We're done interpolating between these frames, so swap buffers
          // and read the next frame out.
          Frame* tmp = current_frame;
          current_frame = next_frame;
          next_frame = tmp;
          interpolation_pos -= (interpolation_frames + 1);
          return this->source->read(*next_frame);
        }
        return true;
    }

    void reset_state() override {
        interpolation_pos = 0;
        this->source->read(*current_frame);
        this->source->read(*next_frame);
    }
private:
    const unsigned int frame_bytes = Frame::frame_bytes();
    const unsigned int interpolation_frames = 0;
    const unsigned int step_size = 0;
    unsigned int interpolation_pos = 0;

    Frame frame_a;
    Frame frame_b;
    Frame* current_frame;
    Frame* next_frame; 
};


template<typename Frame>
class ConsoleDisplay : public Tee<Frame> {
public:
    ConsoleDisplay(shared_ptr<Source<Frame>> s) : Tee<Frame>(s) {};

    bool write(const Frame& frame) override {
        cout << "[ Frame ]" << endl << " { ";
        int i;
        for (i = 0; i < Frame::frame_bytes() - 1; ++i) {
            cout << (int)frame[i] << ", ";
        }
        cout << (int)frame[i];
        cout << " }" << endl;
    }
};

template<int m, int n>
class ConsoleDisplay<RectangularFrame<m, n>> :
    public Tee<RectangularFrame<m, n>> {
public:
    using Frame = RectangularFrame<m, n>;
    ConsoleDisplay(shared_ptr<Source<Frame>> s) : Tee<Frame>(s) {};

    bool write(const Frame& frame) override {
        cout << "[ Frame ]" << " {" << endl;
        int i;
        for (i = 0; i < n; ++i) {
            int j;
            for (j = 0; j < m * 3 - 1; ++j) {
                cout << " " << (const int)frame[j + m * i] << ",";
            }
            cout << " " << (const int)frame[j + m * i] << endl;
        }
        cout << "}" << endl;
        return true;
    }
};


template<typename Frame>
class Multiplier : public Source<Frame> {
public:
    Multiplier(shared_ptr<Source<Frame>> s, int m) :
        Source<Frame>(s), mult(m) {};
    
    bool read(Frame& frame) override {
        if (!this->source->read(frame)) return false;
        for (int i = 0; i < Frame::frame_bytes(); ++i) {
            frame[i] *= mult;
        }
        return true;
    }

    void set_mult(int m) { mult = m; }
private:
    int mult;
};
